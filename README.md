# Install

Go to the module installation page on the setup screen and using the following manifest URL: https://gitlab.com/asacolips-projects/foundry-mods/foundryvtt-mods/raw/master/module.json

# Uninstall

Delete the files added in the install step and restart Foundry VTT.

# Current Features

## Anvil Menu

Click the Anvil logo will open up the player menu, in addition to the default behavior (escape key).

## Chat Notifications

![Screenshot of chat notifications](https://i.imgur.com/CZpa0Iv.jpg)

A very simple notifications area that will display chat log messages in the bottom left corner of your screen if a new message comes through and you were not already viewing the chat log. Interactive elments (buttons, icons) in chat messages will be hidden.

Notifications can be manually closed, or they will automatically close after 5 seconds. There isn't currently a setting to opt-out of notifications, but that feature is planned.