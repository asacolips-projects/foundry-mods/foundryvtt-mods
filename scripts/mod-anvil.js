var modReflow;
var modHideFast;
var modHideSlow;

$(document).ready(() => {
  'use strict';
  /////////////////////////////////////
  // ANVIL MENU ///////////////////////
  /////////////////////////////////////
  $('#logo').on('click', ev => {
    ui.menu.toggle();
  });

  /////////////////////////////////////
  // CHAT POPUPS //////////////////////
  /////////////////////////////////////
  let $chatMarkup = $('<div id="chat-popups" class="chat-empty"><a class="button chat-close"><i class="fas fa-times"></i><div class="visually-hidden">Close popup messages.</div></a><div class="chat-log"><ol class="chat-messages"></ol></div></div>');
  $('#players').after($chatMarkup);

  // Give chat a little bit longer to join the DOM.
  setTimeout(() => {
    // Listen for new chat messages.
    $('body').arrive('#chat-log .message', function() {
      // If we're currently viewing chat, exit early.
      if ($('#chat').hasClass('active')) {
        return;
      }

      // Get chat messages region.
      let $chat = $(this);
      let $chatPopups = $('#chat-popups');
      let $chatMessages = $chatPopups.find('.chat-messages');

      // Append the latest message.
      $chatMessages.append('<li class="message">' + $chat.html() + '</li>');
      $chatPopups.removeClass('chat-empty');
      $chatPopups.removeClass('chat-hide');

      // Clear old timeouts.
      clearTimeout(modHideFast);
      clearTimeout(modHideSlow);

      // Trigger reflow;
      $chatPopups.removeClass('chat-show');
      modReflow = setTimeout(() => {
        $chatPopups.addClass('chat-show');
      }, 1);

      // Hide the popup manually.
      $chatPopups.find('.chat-close').on('click', function(event) {
        // Add the class to animate the hide.
        $chatPopups.addClass('chat-hide');

        // Remove markup and classes after 250ms.
        modHideFast = setTimeout(() => {
          $chatPopups.removeClass('chat-hide');
          $chatPopups.addClass('chat-empty');
          $chatMessages.html('');
        }, 250);
      });

      // If no other action was performed, remove markup and
      // classes after 5s.
      modHideSlow = setTimeout(() => {
        $chatPopups.removeClass('chat-show');
        $chatPopups.addClass('chat-empty');
        $chatMessages.html('');
      }, 5000);
    });
    // End our manual delay.
  }, 2000);
});

Hooks.once("ready", async function() {
  /////////////////////////////////////
  // THAT DAMN NOTIFICATION ///////////
  /////////////////////////////////////
  setTimeout(() => {
    $('.notification.error').each((index, item) => {
      if ($(item).text().includes('requires a minimum screen resolution')) {
        $(item).remove();
      }
    });
  }, 250);
});

Hooks.on('preUpdateToken', (scene, token, data, options, id) => {
  if (data.actorData) {
    if (data.actorData.img) {
      data.img = data.actorData.img;
    }

    if (data.actorData.name) {
      data.name = data.actorData.name;
    }
  }
});

Hooks.on('preUpdateActor', (actor, data, options, id) => {
  // Update the prototype token.
  if (data.img || data.name) {
    if (data.img) {
      data['token.img'] = data.img;
      actor.data.token.img = data.img;
    }

    if (data.name) {
      data['token.name'] = data.name;
      actor.data.token.name = data.name;
    }

    // Update linked tokens too.
    if (actor.data.token.actorLink) {
      let tokens = actor.getActiveTokens();
      let tokenData = {};
      if (data.img) {
        tokenData.img = data.img;
      }

      if (data.name) {
        tokenData.name = data.name;
      }

      tokens.forEach(token => {
        token.update(tokenData);
      });
    }
    // actor.getActiveTokens().forEach(token => token._onUpdateBaseActor(actor.data, data));
  }
});

function maUpdateActorFromToken(tokens = [], mode = 'update') {
  if (tokens.length < 1) {
    tokens = canvas.tokens.controlled;
  }

  if (!['update', 'create'].includes(mode)) {
    ui.notifications.error('Invalid token update mode specified. Use "update" or "create" as your mode.');
  }

  if (tokens.length > 0) {
    let updatedActors = [];
    // Handle all selected tokens.
    tokens.forEach(token => {
      // Avoid duplicate updates.
      if (!updatedActors.includes(token.actor.data._id)) {
        // Clone the token data for update/create.
        let updateData = duplicate(token.actor.data);
        // Handle data adjustments needed for the token.
        updateData = mergeObject(updateData, duplicate(token.data.actorData));
        updateData.token = duplicate(token.data);
        updateData.token.name = updateData.name;
        // Update the associated token if this in the 'update' mode.
        if (mode == 'update') {
          game.actors.get(token.actor.data._id).update(updateData);
          updatedActors.push(token.actor.data._id);
        }
        // Create a new token if in the 'create' mode.
        else if (mode == 'create') {
          // Update the name to show it's a duplicate.
          updateData.name = `${updateData.name} (Copied from Token)`;
          updateData.token.name = updateData.name;
          updateData.token.actorData.name = updateData.name;
          // Clean the id.
          delete updateData._id;
          // Create the actor.
          Actor.create(updateData);
          updatedActors.push(token.actor.data._id);
        }
      }
    });
  }
}